@if(Session::has('flash_message'))
    <div class="alert alertA alert-success alert-dismissible animated fadeInDown" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
@elseif(Session::has('error_message'))
    <div class="alert alertA alert-danger alert-dismissible animated fadeInDown" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('error_message') }}
    </div>
@endif