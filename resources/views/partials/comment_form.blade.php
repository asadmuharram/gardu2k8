<form action="{{ $action }}" method="POST">
    {!! csrf_field() !!}

    <input type="hidden" class="form-control" name="user_id" type="text" value="{{Auth::user()->id}}">

    <div class="form-group">
        <label for="body">Comment</label>
        <textarea class="form-control" name="body"></textarea>
    </div>

    <div class="form-group">
        <button class="btn btn-default" type="submit">Reply</button>
    </div>
</form>