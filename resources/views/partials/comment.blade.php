<div class="panel panel-default">
    <div class="panel-body">
        <?php
        $User = App\User::where('id',$comment->user_id)->first();
        ?>
        <div class="comment-section">
            <div class="left-right">
                <p style="margin-bottom: 10px !important"><b><a href="{{url('/dashboard/user/'.$User->username)}}">{{ $User->name }}</a></b> {{ $comment->body }}</p>
                <p style="margin-bottom: 10px !important"><span style="color: #cccccc;">{{ $comment->created_at->diffForHumans() }}</span> - <a class="btn-default reply-link" data-url="{{ route('comments.reply', [$comment->id]) }}">Balas</a></p>
            </div>
        </div>
    </div>
</div>
@if($comment->hasChildren())
@foreach($comment->getChildren() as $child)
@include('partials.comment_child', ['comment' => $child])
@endforeach
@endif