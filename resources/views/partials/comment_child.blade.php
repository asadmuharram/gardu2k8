<?php 
    $UserParent = App\User::where('id',$comment->parent->user_id)->first(); 
?>
<div class="panel panel-default col-md-offset-{{ $comment->depth }}">
    <div class="panel-body">
        <?php 
            $User = App\User::where('id',$comment->parent_id)->first(); 
        ?>
        <div class="right-comment">
        <p style="margin-bottom: 10px !important"><b><a href="{{url('/dashboard/user/'.$User->username)}}">{{ $User->name }}</a></b> {{ $comment->body }} </p>
        <p style="margin-bottom: 10px !important"><span style="color: #cccccc;">{{ $comment->created_at->diffForHumans() }}</span> - <a class="btn-default reply-link" data-url="{{ route('comments.reply', [$comment->id]) }}">Reply</a></p>
        </div>
    </div>
</div>

@if($comment->hasChildren())
    @foreach($comment->getChildren() as $child)
        @include('partials.comment_child', ['comment' => $child])
    @endforeach
@endif

