@extends('layouts.app')
@section('content')
@include('common.errors')
@include('common.notifications')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        <div class="col-md-12">
            <h3>Pengumuman</h3>
        </div>
    </div>
    <hr/>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <div class="media">
                <div class="media-body">
                    <h4 class="media-heading">{{$News->subject}}</h4>
                    <p class="lead">{!! $News->body !!}</p>
                </div>
            </div>
            <hr/>
            <h5>New comment</h5>
            @include('partials.comment_form', ['action' => route('news.comment', [$News->id])])
            <h2>Comments</h2>
            @if($News->comments()->count() > 0)
            @foreach($News->comments as $comment)
            @include('partials.comment', ['comment' => $comment])
            @endforeach
            @else
            <p>Tidak ada komentar.</p>
            @endif
        </div>
    </div>
</div>
@endsection