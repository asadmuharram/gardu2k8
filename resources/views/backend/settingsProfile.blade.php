@extends('layouts.app')
@section('content')
@include('common.errors')
@include('common.notifications')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        @include('layouts.settingmenu')
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-settings" method="POST" action="{{ url('/dashboard/settings/profile/update') }}" enctype="multipart/form-data" accept-charset="utf-8">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Profil</h3>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $User->name }}" placeholder="Nama Lengkap">
                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('name') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('nickname') ? ' has-error' : '' }}">
                                        <input id="nickname" type="text" class="form-control" name="nickname" value="{{ $User->nickname }}" placeholder="Nama Panggilan">
                                        @if ($errors->has('nickname'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('nickname') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        {{ Form::select('class', [
                                        '3ipa1' => '3-IPA-1',
                                        '3ipa2' => '3-IPA-2',
                                        '3ipa3' => '3-IPA-3',
                                        '3ips1' => '3-IPS-1',
                                        '3ips2' => '3-IPS-2',
                                        '3ips3' => '3-IPS-3',
                                        '3ips4' => '3-IPS-4',] , $User->class, ['placeholder' => 'Pilih Kelas' , 'class' => 'form-control']
                                        ) }}
                                        @if ($errors->has('class'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('class') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <input id="phone" type="text" class="form-control" name="phone" value="{{ $User->phone }}" placeholder="No. Telepon">
                                        @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('phone') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        {{ Form::textarea('address', $User->address[0]->address, ['class' => 'form-control','placeholder' => 'Alamat Lengkap','size' => '30x5']) }}
                                        @if ($errors->has('address'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('address') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Kota | contoh: 'jakarta selatan'" value="{{ $User->address[0]->city->city_name}}, {{$User->address[0]->city->province->province_name }}"/>
                                        <input id="city" type="hidden" class="form-control form-control-sm" name="city" placeholder="Ketik lokasi kota" value="{{ $User->address[0]->city_id }}"/>
                                        @if ($errors->has('city'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('city') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                        <input id="zipcode" type="text" class="form-control" name="zipcode" value="{{ $User->address[0]->zipcode }}" placeholder="Kodepos">
                                        @if ($errors->has('zipcode'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('zipcode') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                        <label for="password-confirm" >Foto</label><br/>
                                        <img src="{{ URL::asset('uploads/photos/'.$User->photo_path) }}" alt="" class="img-responsive" width="10%" height="auto">
                                        <br/>
                                        <input type="file" id="photo" name="photo" value="{{ old('photo') }}">
                                        <small>Pastikan format gambar .jpg / .png dengan ukuran max 1MB.</small>
                                        @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('photo') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary center-block">
                                        <i class="fa fa-btn fa-pencil"></i> Ubah
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection