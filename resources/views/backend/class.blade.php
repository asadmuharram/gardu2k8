@extends('layouts.app')
@section('content')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        @include('layouts.classmenu')
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-12">
                    <h3>{{strtoupper($classId)}}</h3>
                </div>
            </div>
            <hr/>
            <div class="row top-buffer">
                <div class="col-sm-12">
                @foreach($Users->chunk(2) as $items)
                    <div class="row">
                    @foreach($items as $User)
                    <div class="col-sm-6">
                    <div class="media">
                        <a class="media-left media-middle" href="#">
                            <img class="img-circle" src="{{ URL::asset('uploads/photos/'.$User->photo_path) }}" alt="Generic placeholder image">
                        </a>
                        <div class="media-body">
                            <a class="nav-link" href="/dashboard/user/{{$User->username}}"><h6 class="media-heading">{{$User->name}}</h6></a><br/>
                            <small>{{$User->address[0]->city->city_name}}<br/> {{$User->address[0]->city->province->province_name}}</small>
                        </div>
                    </div>
                    </div>
                    @endforeach
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection