@extends('layouts.app')
@section('content')
@include('common.errors')
@include('common.notifications')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        @include('layouts.settingmenu')
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-settings" method="POST" action="{{ url('/dashboard/settings/account/update') }}" enctype="multipart/form-data" accept-charset="utf-8">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Akun</h3>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <input id="username" type="text" class="form-control" name="username" value="{{ $User->username }}" placeholder="Username">
                                        @if ($errors->has('username'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('username') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                        <input id="password" type="oldpassword" class="form-control" name="oldpassword" placeholder="Password saat ini">
                                        @if ($errors->has('oldpassword'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('oldpassword') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password Baru">
                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('password') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password Baru">
                                        @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('password_confirmation') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary center-block">
                                        <i class="fa fa-btn fa-pencil"></i> Ubah
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection