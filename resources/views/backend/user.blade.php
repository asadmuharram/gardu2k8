@extends('layouts.app')
@section('content')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        @include('layouts.classmenu')
        <div class="col-sm-8">
            <div class="row text-xs-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ URL::asset('uploads/photos/'.$User->photo_path) }}" alt="Card image cap">
                        <div class="card-block">
                            <h5 class="card-title">{{$User->name}} ({{$User->nickname}}) <!--<span style="font-size: .9rem;color: #95a5a6"><i class="fa fa-check-circle" aria-hidden="true"></i></span>--></h5>
                            <p class="card-text"><i class="fa fa-map-marker" aria-hidden="true" data-toggle="tooltip"></i> {{$User->address[0]->address}}<br/>{{$User->address[0]->city->city_name}}, {{$User->address[0]->city->province->province_name}}</p>
                            <p><i class="fa fa-phone" aria-hidden="true"></i> {{$User->phone}} | <i class="fa fa-graduation-cap" aria-hidden="true"></i> {{strtoupper($User->class)}}</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="{{ URL::previous() }}" class="card-link"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Kembali</a></li>
                        </ul>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection