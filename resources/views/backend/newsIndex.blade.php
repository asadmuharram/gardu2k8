@extends('layouts.app')
@section('content')
@include('common.errors')
@include('common.notifications')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        <div class="col-md-6">
            <h3>Pengumuman</h3>
        </div>
        <div class="col-md-6 text-xs-right">
            <a href="/dashboard/news/create"><button type="submit" class="btn btn-success btn-sm">
                            <i class="fa fa-btn fa-plus"></i> Buat
                        </button></a>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Publish</th>
                        <th>View</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    ?>
                    @foreach($News as $Item)
                    <tr>
                        <th scope="row">{{$no}}</th>
                        <td>{{$Item->subject}}</td>
                        <td>
                        @if($Item->publish == 'y')
                            Ya
                        @else 
                            Tidak
                        @endif
                        </td>
                        <?php  $idEnc = Crypt::encrypt($Item->id);?>
                        <td><a href="/dashboard/news/{{$Item->id}}"><button type="button" class="btn btn-primary btn-sm">
                            <i class="fa fa-btn fa-eye"></i> Lihat
                        </button></a></td>
                        <td><a href="/dashboard/news/{{$Item->id}}/edit"><button type="button" class="btn btn-primary btn-sm">
                            <i class="fa fa-btn fa-pencil"></i> Ubah
                        </button></a></td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach()
                </tbody>
            </table>
        </div>
    </div>
    <div class="row-height">
        <div class="col-md-12 col-xs-12 text-left">
        {!! $News->render() !!}
        </div>
        </div>
</div>
@endsection