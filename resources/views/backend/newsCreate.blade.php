@extends('layouts.app')
@section('content')
@include('common.errors')
@include('common.notifications')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        <div class="col-sm-12">
                    <form class="form-settings" method="POST" action="{{ url('/dashboard/news/store') }}" accept-charset="utf-8">
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Pengumuman</h3>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" placeholder="Nama Pengumuman">
                                        @if ($errors->has('subject'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('subject') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                        {{ Form::textarea('body', null, ['class' => 'form-control textDesc','placeholder' => 'Isi pengumuman.','size' => '30x5']) }}
                                        @if ($errors->has('body'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('body') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('publish') ? ' has-error' : '' }}">
                                        {{ Form::select('publish', [
                                        'y' => 'Ya',
                                        'n' => 'Tidak'] , null, ['placeholder' => 'Publish Sekarang ?' , 'class' => 'form-control']
                                        ) }}
                                        @if ($errors->has('publish'))
                                        <span class="help-block">
                                            <small>{{ $errors->first('publish') }}</small>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary pull-xs-right">
                                        <i class="fa fa-btn fa-pencil"></i> Buat
                                        </button>
                                </div>
                                <div class="col-md-6">
                                        <a href="{{ URL::previous() }}"><button type="button" class="btn btn-secondary left-block">
                                        Batal
                                        </button></a>
                                </div>
                            </div>
                        </fieldset>
                    </form>
        </div>
    </div>
</div>
@endsection