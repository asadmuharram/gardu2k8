@extends('layouts.app')
@section('content')
<!-- Begin page content -->
<div class="container">
    @include('layouts.topmenu')
    @include('layouts.search')
    <div class="row">
        @include('layouts.classmenu')
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-block text-xs-center">
                        <h4 class="card-title">Kamu tergabung di kelas <span style="font-weight: bold">{{strtoupper($User->class)}}</span></h4>
                        <p class="card-text">
                            @if($UserClassCount > 0)
                            Terdapat {{$UserClassCount}} teman dikelas ini.
                            @else
                            Belum ada teman dikelas ini.
                            @endif
                        </p>
                        @if($UserClassCount > 0)
                        <a href="#" class="btn btn-primary">Lihat teman lainnya <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection