@extends('layouts.app')
@include('common.errors')
@include('common.notifications')
@section('content')

<!-- Home content -->
<div class="home-content">
    <div class="container">
        <div class="home-text">
            <div class="row">
                <div class="col-md-12">
                    <h3><span style="font-weight: bold">Mohon maaf</span> kamu tidak dapat mengakses website ini. <i class="fa fa-hand-peace-o" aria-hidden="true"></i></small></h3>
                </div>
            </div>
        </div>
    </div>
    </div>

    @endsection