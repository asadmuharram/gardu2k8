@extends('layouts.app')
@include('common.errors')
@include('common.notifications')
@section('content')

<!-- Home content -->
<div class="home-content">
    <div class="container">
        <div class="home-text">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="lead">Selamat Datang Alumni <span style="font-weight: bold">SMAN 109 Jakarta</span></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="/yearcheck" method="post" accept-charset="utf-8">
                        {!! csrf_field() !!}
                        <fieldset>
                            <div class="form-group">
                                <label for="exampleSelect1">Untuk melanjutkan menggunakan website ini, Silahkan pilih tahun kelulusan :</label>
                                <select class="form-control" id="exampleSelect1" name="year">
                                    <option value="2006">Tahun 2006</option>
                                    <option value="2007">Tahun 2007</option>
                                    <option value="2008">Tahun 2008</option>
                                    <option value="2009">Tahun 2009</option>
                                    <option value="2010">Tahun 2010</option>
                                </select>
                                <small class="text-muted">* <span style="font-weight: bold;">Perhatian</span>  pastikan tahun kelulusan yang kamu pilih benar.
                            </div>
                            <button type="submit" class="btn btn-primary center-block">Submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    @endsection