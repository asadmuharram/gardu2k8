<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Gardu2K8 - SMAN 109</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet"/>
        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}" rel="stylesheet"/>
        <style>
        body {
        font-family: 'Lato';
        }
        </style>
    </head>
    <body>
        <div id="wrapper">
        <div id="content">
        @yield('content')
        </div>
        <footer class="footer">
            <div class="container">
                    <p class="text-md-center text-xs-center">2016 Gardu2K8.com - Website by Vnvmedia</p>
            </div>
        </footer>
        </div>
        <!-- JavaScripts -->
        <script src="https://use.fontawesome.com/fb50727867.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.3.1/js/tether.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
        <script src="{{ URL::asset('assets/js/tinymce/jquery.tinymce.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js') }}"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
        <script>
        $(document).ready(function() {
        $(document).ready(function(){window.setTimeout(function(){$(".alertA").fadeTo(1e3,0).slideUp(200,function(){$(this).remove()})},3e3)}),
        $("[data-toggle='tooltip']").tooltip(), $("#city_id_search").autocomplete({
        minLength: 2,
        select: function(e, t) {
        return $("#city_id_search").val(t.item.value), $("#city_search").val(t.item.id), $("#searchLocation").submit(), !1
        },
        source: function(e, t) {
        $.ajax({
        url: '{{ url("/getcities") }}',
        data: {
        _token: secure_token
        },
        data: {
        term: e.term
        },
        dataType: "json",
        success: function(e) {
        t($.map(e, function(e) {
        return console.log(e.city_name), {
        id: e.city_id,
        value: e.city_name
        }
        }))
        }
        })
        }
        }), $("#city_id").autocomplete({
        minLength: 2,
        select: function(e, t) {
        return $("#city_id").val(t.item.value), $("#city").val(t.item.id), !1
        },
        source: function(e, t) {
        $.ajax({
        url: '{{ url("getcities") }}',
        data: {
        term: e.term
        },
        dataType: "json",
        success: function(e) {
        t($.map(e, function(e) {
        return console.log(e.city_name), {
        id: e.city_id,
        value: e.city_name
        }
        }))
        }
        })
        }
        })
        });
        </script>
        <script>
            $(document).ready(function() {
            $('.reply-link').click(createForm);
            function createForm(e)
            {
            e.preventDefault();
            var form = [];
            form[form.length] = '<form class="reply-form" action="' + $(this).data('url')
                + '" method="post">';
                form[form.length] = '   {!! csrf_field() !!}';
                form[form.length] = '   <div class="form-group">';
                    form[form.length] = '       <label for="body">Comment</label>';
                    form[form.length] = '       <textarea class="form-control" name="body"></textarea>';
                form[form.length] = '   </div>';
                form[form.length] = '   <div class="form-group">';
                    form[form.length] = '       <button class="btn btn-default" type="submit">Reply</button>';
                form[form.length] = '   </div>';
            form[form.length] = '</form>';
            $(this).replaceWith(form.join(''));
            }
            });
        </script>
        <script>
        tinymce.init({
        selector: '.textDesc',
        toolbar: [
        'undo | redo | cut | copy | paste | bold | italic | underline | bullist | numlist | outdent | indent | removeformat '
        ],
        menu: {
        
        },
        statusbar: false,
        theme: "modern",
        skin: 'light',
        plugins : "paste",
        paste_auto_cleanup_on_paste : true,
        });
        </script>
    </body>
</html>