<div class="col-sm-4">
    <div class="list-group">
        <a href="#" class="list-group-item active">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>   Daftar Kelas
        </a>
        <a href="/dashboard/class/3ipa1" class="list-group-item">
        <?php
            $User = App\User::where('class','3ipa1')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>
        3IPA1</a>
        <a href="/dashboard/class/3ipa2" class="list-group-item">
        <?php
            $User = App\User::where('class','3ipa2')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>3IPA2</a>
        <a href="/dashboard/class/3ipa3" class="list-group-item">
        <?php
            $User = App\User::where('class','3ipa3')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>3IPA3</a>
        <a href="/dashboard/class/3ips1" class="list-group-item">
        <?php
            $User = App\User::where('class','3ips1')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>3IPS1</a>
        <a href="/dashboard/class/3ips2" class="list-group-item">
        <?php
            $User = App\User::where('class','3ips2')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>3IPS2</a>
        <a href="/dashboard/class/3ips3" class="list-group-item">
        <?php
            $User = App\User::where('class','3ips3')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>3IPS3</a>
        <a href="/dashboard/class/3ips4" class="list-group-item">
        <?php
            $User = App\User::where('class','3ips4')->get();
        ?>
        <span class="label label-default label-pill pull-xs-right">{{count($User)}} Teman</span>3IPS4</a>
    </div>
</div>