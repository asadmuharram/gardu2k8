<div class="header clearfix">
        <nav>
           <ul class="nav nav-pills pull-xs-left">
                <li class="nav-item">
                    <a href="/dashboard" class="nav-link"><h3>Gardu2K8</h3></a>
                </li>
            </ul>
            <ul class="nav nav-pills pull-xs-right">
                <li class="nav-item">
                    <a class="nav-link" href="#">halo, {{strtolower(strtok(Auth::user()->nickname, " "))}} </a>
                </li>
                <li class="nav-item">
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item" href="/dashboard/settings/account">Pengaturan</a>
                                @if(Auth::user()->role == 'admin')
                                <a class="dropdown-item" href="/dashboard/news">Pengumuman</a>
                                @endif
                                <a class="dropdown-item" href="/logout">Keluar</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>
    </div>