<div class="row">
    <div class="col-sm-12">
            <?php
            $News = App\News::where('publish','y')->first();

            if(!empty($News)){
            $string = strip_tags($News->body);
            $yourText = $News->body;
            if (strlen($string) > 255) {
            $stringCut = substr($News->body, 0, 255);
            $doc = new DOMDocument();
            $doc->loadHTML($stringCut);
            $yourText = $doc->saveHTML();
            }
            ?>
            <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5><strong>Pengumuman :</strong> {{$News->subject}} - <small>{{$News->created_at}}</small></h5>
            <p>{!! $yourText !!}</p>
            <hr/>
            <a style="font-weight: bold;position:relative;color: #31708f" href="/dashboard/news/{{$News->id}}">Selengkapnya</a>
            </div>
            <?php
            } 
            ?>

        
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="search-top">
            <form method="get" action="{{ url('/dashboard/search') }}" accept-charset="utf-8">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="ketik nama teman. . ." name="keyword">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Cari</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>