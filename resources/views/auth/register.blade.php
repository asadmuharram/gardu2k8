@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="form-register" method="POST" action="{{ url('/register') }}" enctype="multipart/form-data" accept-charset="utf-8">
                {{ csrf_field() }}
                <h3>Daftar</h3>
                <fieldset>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Lengkap">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <small>{{ $errors->first('name') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('nickname') ? ' has-error' : '' }}">
                                <input id="nickname" type="text" class="form-control" name="nickname" value="{{ old('nickname') }}" placeholder="Nama Panggilan">
                                @if ($errors->has('nickname'))
                                <span class="help-block">
                                    <small>{{ $errors->first('nickname') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username">
                                @if ($errors->has('username'))
                                <span class="help-block">
                                    <small>{{ $errors->first('username') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                {{ Form::select('class', [
                                '3ipa1' => '3-IPA-1',
                                '3ipa2' => '3-IPA-2',
                                '3ipa3' => '3-IPA-3',
                                '3ips1' => '3-IPS-1',
                                '3ips2' => '3-IPS-2',
                                '3ips3' => '3-IPS-3',
                                '3ips4' => '3-IPS-4',] , null, ['placeholder' => 'Pilih Kelas' , 'class' => 'form-control']
                                ) }}
                                @if ($errors->has('class'))
                                <span class="help-block">
                                    <small>{{ $errors->first('class') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Alamat Email">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <small>{{ $errors->first('email') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="No. Telepon">
                                @if ($errors->has('phone'))
                                <span class="help-block">
                                    <small>{{ $errors->first('phone') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <small>{{ $errors->first('password') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <small>{{ $errors->first('password_confirmation') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                {{ Form::textarea('address', null, ['class' => 'form-control','placeholder' => 'Alamat Lengkap','size' => '30x5']) }}
                                @if ($errors->has('address'))
                                <span class="help-block">
                                    <small>{{ $errors->first('address') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Kota | contoh: 'jakarta selatan'" value="{{ old('city_id') }}"/>
                                <input id="city" type="hidden" class="form-control form-control-sm" name="city" placeholder="Ketik lokasi kota" value="{{ old('city') }}"/>
                                @if ($errors->has('city'))
                                <span class="help-block">
                                    <small>{{ $errors->first('city') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                <input id="zipcode" type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}" placeholder="Kodepos">
                                @if ($errors->has('zipcode'))
                                <span class="help-block">
                                    <small>{{ $errors->first('zipcode') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                <label for="password-confirm" >Foto</label><br/>
                                <input type="file" id="photo" name="photo" value="{{ old('photo') }}">
                                <small>Pastikan format gambar .jpg / .png dengan ukuran max 1MB.</small>
                                @if ($errors->has('photo'))
                                <span class="help-block">
                                    <small>{{ $errors->first('photo') }}</small>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! app('captcha')->display(); !!}
                            </div>
                        </div>
                    </div>
                    <div class="row top-buffer">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary center-block">
                                <i class="fa fa-btn fa-user"></i> Daftar
                                </button>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <p class="top-buffer">Sudah punya akun ? <a class="btn-link" href="{{ url('/login') }}">Masuk</a></p>
                </fieldset>
            </form>
            
        </div>
    </div>
</div>
@endsection