@extends('layouts.app')
@section('content')
@include('common.notifications')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-login" method="POST" action="{{ url('/login') }}" accept-charset="utf-8">
                {{ csrf_field() }}
                <h3>Login</h3>
                <fieldset>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Alamat Email">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <small>{{ $errors->first('email') }}</small>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <small>{{ $errors->first('password') }}</small>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary center-block">
                        <i class="fa fa-btn fa-sign-in"></i> Masuk
                        </button>
                    </div>
                    <hr/>
                    <!--<a class="btn-link" href="{{ url('/password/reset') }}">Lupa password ?</a>-->
                    <p class="top-buffer">Belum punya akun ? <a class="btn-link" href="{{ url('/register') }}">Daftar</a></p>
                </fieldset>

            </form>
        </div>
    </div>
</div>

@endsection