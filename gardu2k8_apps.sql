/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : gardu2k8_apps

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-06-15 11:00:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addresses
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `addresses_user_id_unique` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of addresses
-- ----------------------------

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cities_city_name_unique` (`city_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9472 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES ('1101', 'Kabupaten Simeulue', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1102', 'Kabupaten Aceh Singkil', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1103', 'Kabupaten Aceh Selatan', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1104', 'Kabupaten Aceh Tenggara', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1105', 'Kabupaten Aceh Timur', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1106', 'Kabupaten Aceh Tengah', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1107', 'Kabupaten Aceh Barat', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1108', 'Kabupaten Aceh Besar', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1109', 'Kabupaten Pidie', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1110', 'Kabupaten Bireuen', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1111', 'Kabupaten Aceh Utara', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1112', 'Kabupaten Aceh Barat Daya', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1113', 'Kabupaten Gayo Lues', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1114', 'Kabupaten Aceh Tamiang', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1115', 'Kabupaten Nagan Raya', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1116', 'Kabupaten Aceh Jaya', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1117', 'Kabupaten Bener Meriah', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1118', 'Kabupaten Pidie Jaya', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1171', 'Kota Banda Aceh', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1172', 'Kota Sabang', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1173', 'Kota Langsa', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1174', 'Kota Lhokseumawe', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1175', 'Kota Subulussalam', '11', '1', null, null);
INSERT INTO `cities` VALUES ('1201', 'Kabupaten Nias', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1202', 'Kabupaten Mandailing Natal', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1203', 'Kabupaten Tapanuli Selatan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1204', 'Kabupaten Tapanuli Tengah', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1205', 'Kabupaten Tapanuli Utara', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1206', 'Kabupaten Toba Samosir', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1207', 'Kabupaten Labuhan Batu', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1208', 'Kabupaten Asahan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1209', 'Kabupaten Simalungun', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1210', 'Kabupaten Dairi', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1211', 'Kabupaten Karo', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1212', 'Kabupaten Deli Serdang', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1213', 'Kabupaten Langkat', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1214', 'Kabupaten Nias Selatan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1215', 'Kabupaten Humbang Hasundutan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1216', 'Kabupaten Pakpak Bharat', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1217', 'Kabupaten Samosir', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1218', 'Kabupaten Serdang Bedagai', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1219', 'Kabupaten Batu Bara', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1220', 'Kabupaten Padang Lawas Utara', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1221', 'Kabupaten Padang Lawas', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1222', 'Kabupaten Labuhan Batu Selatan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1223', 'Kabupaten Labuhan Batu Utara', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1224', 'Kabupaten Nias Utara', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1225', 'Kabupaten Nias Barat', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1271', 'Kota Sibolga', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1272', 'Kota Tanjung Balai', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1273', 'Kota Pematang Siantar', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1274', 'Kota Tebing Tinggi', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1275', 'Kota Medan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1276', 'Kota Binjai', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1277', 'Kota Padangsidimpuan', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1278', 'Kota Gunungsitoli', '12', '1', null, null);
INSERT INTO `cities` VALUES ('1301', 'Kabupaten Kepulauan Mentawai', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1302', 'Kabupaten Pesisir Selatan', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1303', 'Kabupaten Solok', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1304', 'Kabupaten Sijunjung', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1305', 'Kabupaten Tanah Datar', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1306', 'Kabupaten Padang Pariaman', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1307', 'Kabupaten Agam', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1308', 'Kabupaten Lima Puluh Kota', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1309', 'Kabupaten Pasaman', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1310', 'Kabupaten Solok Selatan', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1311', 'Kabupaten Dharmasraya', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1312', 'Kabupaten Pasaman Barat', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1371', 'Kota Padang', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1372', 'Kota Solok', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1373', 'Kota Sawah Lunto', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1374', 'Kota Padang Panjang', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1375', 'Kota Bukittinggi', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1376', 'Kota Payakumbuh', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1377', 'Kota Pariaman', '13', '1', null, null);
INSERT INTO `cities` VALUES ('1401', 'Kabupaten Kuantan Singingi', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1402', 'Kabupaten Indragiri Hulu', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1403', 'Kabupaten Indragiri Hilir', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1404', 'Kabupaten Pelalawan', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1405', 'Kabupaten S I A K', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1406', 'Kabupaten Kampar', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1407', 'Kabupaten Rokan Hulu', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1408', 'Kabupaten Bengkalis', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1409', 'Kabupaten Rokan Hilir', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1410', 'Kabupaten Kepulauan Meranti', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1471', 'Kota Pekanbaru', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1473', 'Kota D U M A I', '14', '1', null, null);
INSERT INTO `cities` VALUES ('1501', 'Kabupaten Kerinci', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1502', 'Kabupaten Merangin', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1503', 'Kabupaten Sarolangun', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1504', 'Kabupaten Batang Hari', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1505', 'Kabupaten Muaro Jambi', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1506', 'Kabupaten Tanjung Jabung Timur', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1507', 'Kabupaten Tanjung Jabung Barat', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1508', 'Kabupaten Tebo', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1509', 'Kabupaten Bungo', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1571', 'Kota Jambi', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1572', 'Kota Sungai Penuh', '15', '1', null, null);
INSERT INTO `cities` VALUES ('1601', 'Kabupaten Ogan Komering Ulu', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1602', 'Kabupaten Ogan Komering Ilir', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1603', 'Kabupaten Muara Enim', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1604', 'Kabupaten Lahat', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1605', 'Kabupaten Musi Rawas', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1606', 'Kabupaten Musi Banyuasin', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1607', 'Kabupaten Banyu Asin', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1608', 'Kabupaten Ogan Komering Ulu Selatan', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1609', 'Kabupaten Ogan Komering Ulu Timur', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1610', 'Kabupaten Ogan Ilir', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1611', 'Kabupaten Empat Lawang', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1671', 'Kota Palembang', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1672', 'Kota Prabumulih', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1673', 'Kota Pagar Alam', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1674', 'Kota Lubuklinggau', '16', '1', null, null);
INSERT INTO `cities` VALUES ('1701', 'Kabupaten Bengkulu Selatan', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1702', 'Kabupaten Rejang Lebong', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1703', 'Kabupaten Bengkulu Utara', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1704', 'Kabupaten Kaur', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1705', 'Kabupaten Seluma', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1706', 'Kabupaten Mukomuko', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1707', 'Kabupaten Lebong', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1708', 'Kabupaten Kepahiang', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1709', 'Kabupaten Bengkulu Tengah', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1771', 'Kota Bengkulu', '17', '1', null, null);
INSERT INTO `cities` VALUES ('1801', 'Kabupaten Lampung Barat', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1802', 'Kabupaten Tanggamus', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1803', 'Kabupaten Lampung Selatan', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1804', 'Kabupaten Lampung Timur', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1805', 'Kabupaten Lampung Tengah', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1806', 'Kabupaten Lampung Utara', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1807', 'Kabupaten Way Kanan', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1808', 'Kabupaten Tulangbawang', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1809', 'Kabupaten Pesawaran', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1810', 'Kabupaten Pringsewu', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1811', 'Kabupaten Mesuji', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1812', 'Kabupaten Tulang Bawang Barat', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1813', 'Kabupaten Pesisir Barat', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1871', 'Kota Bandar Lampung', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1872', 'Kota Metro', '18', '1', null, null);
INSERT INTO `cities` VALUES ('1901', 'Kabupaten Bangka', '19', '1', null, null);
INSERT INTO `cities` VALUES ('1902', 'Kabupaten Belitung', '19', '1', null, null);
INSERT INTO `cities` VALUES ('1903', 'Kabupaten Bangka Barat', '19', '1', null, null);
INSERT INTO `cities` VALUES ('1904', 'Kabupaten Bangka Tengah', '19', '1', null, null);
INSERT INTO `cities` VALUES ('1905', 'Kabupaten Bangka Selatan', '19', '1', null, null);
INSERT INTO `cities` VALUES ('1906', 'Kabupaten Belitung Timur', '19', '1', null, null);
INSERT INTO `cities` VALUES ('1971', 'Kota Pangkal Pinang', '19', '1', null, null);
INSERT INTO `cities` VALUES ('2101', 'Kabupaten Karimun', '21', '1', null, null);
INSERT INTO `cities` VALUES ('2102', 'Kabupaten Bintan', '21', '1', null, null);
INSERT INTO `cities` VALUES ('2103', 'Kabupaten Natuna', '21', '1', null, null);
INSERT INTO `cities` VALUES ('2104', 'Kabupaten Lingga', '21', '1', null, null);
INSERT INTO `cities` VALUES ('2105', 'Kabupaten Kepulauan Anambas', '21', '1', null, null);
INSERT INTO `cities` VALUES ('2171', 'Kota B A T A M', '21', '1', null, null);
INSERT INTO `cities` VALUES ('2172', 'Kota Tanjung Pinang', '21', '1', null, null);
INSERT INTO `cities` VALUES ('3101', 'Kabupaten Kepulauan Seribu', '31', '1', null, null);
INSERT INTO `cities` VALUES ('3171', 'Kota Jakarta Selatan', '31', '1', null, null);
INSERT INTO `cities` VALUES ('3172', 'Kota Jakarta Timur', '31', '1', null, null);
INSERT INTO `cities` VALUES ('3173', 'Kota Jakarta Pusat', '31', '1', null, null);
INSERT INTO `cities` VALUES ('3174', 'Kota Jakarta Barat', '31', '1', null, null);
INSERT INTO `cities` VALUES ('3175', 'Kota Jakarta Utara', '31', '1', null, null);
INSERT INTO `cities` VALUES ('3201', 'Kabupaten Bogor', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3202', 'Kabupaten Sukabumi', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3203', 'Kabupaten Cianjur', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3204', 'Kabupaten Bandung', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3205', 'Kabupaten Garut', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3206', 'Kabupaten Tasikmalaya', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3207', 'Kabupaten Ciamis', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3208', 'Kabupaten Kuningan', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3209', 'Kabupaten Cirebon', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3210', 'Kabupaten Majalengka', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3211', 'Kabupaten Sumedang', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3212', 'Kabupaten Indramayu', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3213', 'Kabupaten Subang', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3214', 'Kabupaten Purwakarta', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3215', 'Kabupaten Karawang', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3216', 'Kabupaten Bekasi', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3217', 'Kabupaten Bandung Barat', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3218', 'Kabupaten Pangandaran', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3271', 'Kota Bogor', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3272', 'Kota Sukabumi', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3273', 'Kota Bandung', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3274', 'Kota Cirebon', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3275', 'Kota Bekasi', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3276', 'Kota Depok', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3277', 'Kota Cimahi', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3278', 'Kota Tasikmalaya', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3279', 'Kota Banjar', '32', '1', null, null);
INSERT INTO `cities` VALUES ('3301', 'Kabupaten Cilacap', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3302', 'Kabupaten Banyumas', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3303', 'Kabupaten Purbalingga', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3304', 'Kabupaten Banjarnegara', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3305', 'Kabupaten Kebumen', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3306', 'Kabupaten Purworejo', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3307', 'Kabupaten Wonosobo', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3308', 'Kabupaten Magelang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3309', 'Kabupaten Boyolali', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3310', 'Kabupaten Klaten', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3311', 'Kabupaten Sukoharjo', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3312', 'Kabupaten Wonogiri', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3313', 'Kabupaten Karanganyar', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3314', 'Kabupaten Sragen', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3315', 'Kabupaten Grobogan', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3316', 'Kabupaten Blora', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3317', 'Kabupaten Rembang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3318', 'Kabupaten Pati', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3319', 'Kabupaten Kudus', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3320', 'Kabupaten Jepara', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3321', 'Kabupaten Demak', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3322', 'Kabupaten Semarang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3323', 'Kabupaten Temanggung', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3324', 'Kabupaten Kendal', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3325', 'Kabupaten Batang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3326', 'Kabupaten Pekalongan', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3327', 'Kabupaten Pemalang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3328', 'Kabupaten Tegal', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3329', 'Kabupaten Brebes', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3371', 'Kota Magelang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3372', 'Kota Surakarta', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3373', 'Kota Salatiga', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3374', 'Kota Semarang', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3375', 'Kota Pekalongan', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3376', 'Kota Tegal', '33', '1', null, null);
INSERT INTO `cities` VALUES ('3401', 'Kabupaten Kulon Progo', '34', '1', null, null);
INSERT INTO `cities` VALUES ('3402', 'Kabupaten Bantul', '34', '1', null, null);
INSERT INTO `cities` VALUES ('3403', 'Kabupaten Gunung Kidul', '34', '1', null, null);
INSERT INTO `cities` VALUES ('3404', 'Kabupaten Sleman', '34', '1', null, null);
INSERT INTO `cities` VALUES ('3471', 'Kota Yogyakarta', '34', '1', null, null);
INSERT INTO `cities` VALUES ('3501', 'Kabupaten Pacitan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3502', 'Kabupaten Ponorogo', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3503', 'Kabupaten Trenggalek', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3504', 'Kabupaten Tulungagung', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3505', 'Kabupaten Blitar', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3506', 'Kabupaten Kediri', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3507', 'Kabupaten Malang', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3508', 'Kabupaten Lumajang', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3509', 'Kabupaten Jember', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3510', 'Kabupaten Banyuwangi', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3511', 'Kabupaten Bondowoso', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3512', 'Kabupaten Situbondo', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3513', 'Kabupaten Probolinggo', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3514', 'Kabupaten Pasuruan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3515', 'Kabupaten Sidoarjo', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3516', 'Kabupaten Mojokerto', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3517', 'Kabupaten Jombang', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3518', 'Kabupaten Nganjuk', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3519', 'Kabupaten Madiun', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3520', 'Kabupaten Magetan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3521', 'Kabupaten Ngawi', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3522', 'Kabupaten Bojonegoro', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3523', 'Kabupaten Tuban', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3524', 'Kabupaten Lamongan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3525', 'Kabupaten Gresik', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3526', 'Kabupaten Bangkalan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3527', 'Kabupaten Sampang', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3528', 'Kabupaten Pamekasan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3529', 'Kabupaten Sumenep', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3571', 'Kota Kediri', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3572', 'Kota Blitar', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3573', 'Kota Malang', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3574', 'Kota Probolinggo', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3575', 'Kota Pasuruan', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3576', 'Kota Mojokerto', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3577', 'Kota Madiun', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3578', 'Kota Surabaya', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3579', 'Kota Batu', '35', '1', null, null);
INSERT INTO `cities` VALUES ('3601', 'Kabupaten Pandeglang', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3602', 'Kabupaten Lebak', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3603', 'Kabupaten Tangerang', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3604', 'Kabupaten Serang', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3671', 'Kota Tangerang', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3672', 'Kota Cilegon', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3673', 'Kota Serang', '36', '1', null, null);
INSERT INTO `cities` VALUES ('3674', 'Kota Tangerang Selatan', '36', '1', null, null);
INSERT INTO `cities` VALUES ('5101', 'Kabupaten Jembrana', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5102', 'Kabupaten Tabanan', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5103', 'Kabupaten Badung', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5104', 'Kabupaten Gianyar', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5105', 'Kabupaten Klungkung', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5106', 'Kabupaten Bangli', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5107', 'Kabupaten Karang Asem', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5108', 'Kabupaten Buleleng', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5171', 'Kota Denpasar', '51', '1', null, null);
INSERT INTO `cities` VALUES ('5201', 'Kabupaten Lombok Barat', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5202', 'Kabupaten Lombok Tengah', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5203', 'Kabupaten Lombok Timur', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5204', 'Kabupaten Sumbawa', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5205', 'Kabupaten Dompu', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5206', 'Kabupaten Bima', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5207', 'Kabupaten Sumbawa Barat', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5208', 'Kabupaten Lombok Utara', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5271', 'Kota Mataram', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5272', 'Kota Bima', '52', '1', null, null);
INSERT INTO `cities` VALUES ('5301', 'Kabupaten Sumba Barat', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5302', 'Kabupaten Sumba Timur', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5303', 'Kabupaten Kupang', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5304', 'Kabupaten Timor Tengah Selatan', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5305', 'Kabupaten Timor Tengah Utara', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5306', 'Kabupaten Belu', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5307', 'Kabupaten Alor', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5308', 'Kabupaten Lembata', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5309', 'Kabupaten Flores Timur', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5310', 'Kabupaten Sikka', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5311', 'Kabupaten Ende', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5312', 'Kabupaten Ngada', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5313', 'Kabupaten Manggarai', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5314', 'Kabupaten Rote Ndao', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5315', 'Kabupaten Manggarai Barat', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5316', 'Kabupaten Sumba Tengah', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5317', 'Kabupaten Sumba Barat Daya', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5318', 'Kabupaten Nagekeo', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5319', 'Kabupaten Manggarai Timur', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5320', 'Kabupaten Sabu Raijua', '53', '1', null, null);
INSERT INTO `cities` VALUES ('5371', 'Kota Kupang', '53', '1', null, null);
INSERT INTO `cities` VALUES ('6101', 'Kabupaten Sambas', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6102', 'Kabupaten Bengkayang', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6103', 'Kabupaten Landak', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6104', 'Kabupaten Pontianak', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6105', 'Kabupaten Sanggau', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6106', 'Kabupaten Ketapang', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6107', 'Kabupaten Sintang', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6108', 'Kabupaten Kapuas Hulu', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6109', 'Kabupaten Sekadau', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6110', 'Kabupaten Melawi', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6111', 'Kabupaten Kayong Utara', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6112', 'Kabupaten Kubu Raya', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6171', 'Kota Pontianak', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6172', 'Kota Singkawang', '61', '1', null, null);
INSERT INTO `cities` VALUES ('6201', 'Kabupaten Kotawaringin Barat', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6202', 'Kabupaten Kotawaringin Timur', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6203', 'Kabupaten Kapuas', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6204', 'Kabupaten Barito Selatan', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6205', 'Kabupaten Barito Utara', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6206', 'Kabupaten Sukamara', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6207', 'Kabupaten Lamandau', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6208', 'Kabupaten Seruyan', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6209', 'Kabupaten Katingan', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6210', 'Kabupaten Pulang Pisau', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6211', 'Kabupaten Gunung Mas', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6212', 'Kabupaten Barito Timur', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6213', 'Kabupaten Murung Raya', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6271', 'Kota Palangka Raya', '62', '1', null, null);
INSERT INTO `cities` VALUES ('6301', 'Kabupaten Tanah Laut', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6302', 'Kabupaten Kota Baru', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6303', 'Kabupaten Banjar', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6304', 'Kabupaten Barito Kuala', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6305', 'Kabupaten Tapin', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6306', 'Kabupaten Hulu Sungai Selatan', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6307', 'Kabupaten Hulu Sungai Tengah', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6308', 'Kabupaten Hulu Sungai Utara', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6309', 'Kabupaten Tabalong', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6310', 'Kabupaten Tanah Bumbu', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6311', 'Kabupaten Balangan', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6371', 'Kota Banjarmasin', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6372', 'Kota Banjar Baru', '63', '1', null, null);
INSERT INTO `cities` VALUES ('6401', 'Kabupaten Paser', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6402', 'Kabupaten Kutai Barat', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6403', 'Kabupaten Kutai Kartanegara', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6404', 'Kabupaten Kutai Timur', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6405', 'Kabupaten Berau', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6409', 'Kabupaten Penajam Paser Utara', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6471', 'Kota Balikpapan', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6472', 'Kota Samarinda', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6474', 'Kota Bontang', '64', '1', null, null);
INSERT INTO `cities` VALUES ('6501', 'Kabupaten Malinau', '65', '1', null, null);
INSERT INTO `cities` VALUES ('6502', 'Kabupaten Bulungan', '65', '1', null, null);
INSERT INTO `cities` VALUES ('6503', 'Kabupaten Tana Tidung', '65', '1', null, null);
INSERT INTO `cities` VALUES ('6504', 'Kabupaten Nunukan', '65', '1', null, null);
INSERT INTO `cities` VALUES ('6571', 'Kota Tarakan', '65', '1', null, null);
INSERT INTO `cities` VALUES ('7101', 'Kabupaten Bolaang Mongondow', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7102', 'Kabupaten Minahasa', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7103', 'Kabupaten Kepulauan Sangihe', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7104', 'Kabupaten Kepulauan Talaud', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7105', 'Kabupaten Minahasa Selatan', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7106', 'Kabupaten Minahasa Utara', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7107', 'Kabupaten Bolaang Mongondow Utara', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7108', 'Kabupaten Siau Tagulandang Biaro', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7109', 'Kabupaten Minahasa Tenggara', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7110', 'Kabupaten Bolaang Mongondow Selatan', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7111', 'Kabupaten Bolaang Mongondow Timur', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7171', 'Kota Manado', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7172', 'Kota Bitung', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7173', 'Kota Tomohon', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7174', 'Kota Kotamobagu', '71', '1', null, null);
INSERT INTO `cities` VALUES ('7201', 'Kabupaten Banggai Kepulauan', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7202', 'Kabupaten Banggai', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7203', 'Kabupaten Morowali', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7204', 'Kabupaten Poso', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7205', 'Kabupaten Donggala', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7206', 'Kabupaten Toli-toli', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7207', 'Kabupaten Buol', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7208', 'Kabupaten Parigi Moutong', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7209', 'Kabupaten Tojo Una-una', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7210', 'Kabupaten Sigi', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7271', 'Kota Palu', '72', '1', null, null);
INSERT INTO `cities` VALUES ('7301', 'Kabupaten Kepulauan Selayar', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7302', 'Kabupaten Bulukumba', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7303', 'Kabupaten Bantaeng', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7304', 'Kabupaten Jeneponto', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7305', 'Kabupaten Takalar', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7306', 'Kabupaten Gowa', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7307', 'Kabupaten Sinjai', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7308', 'Kabupaten Maros', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7309', 'Kabupaten Pangkajene Dan Kepulauan', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7310', 'Kabupaten Barru', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7311', 'Kabupaten Bone', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7312', 'Kabupaten Soppeng', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7313', 'Kabupaten Wajo', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7314', 'Kabupaten Sidenreng Rappang', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7315', 'Kabupaten Pinrang', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7316', 'Kabupaten Enrekang', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7317', 'Kabupaten Luwu', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7318', 'Kabupaten Tana Toraja', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7322', 'Kabupaten Luwu Utara', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7325', 'Kabupaten Luwu Timur', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7326', 'Kabupaten Toraja Utara', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7371', 'Kota Makassar', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7372', 'Kota Parepare', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7373', 'Kota Palopo', '73', '1', null, null);
INSERT INTO `cities` VALUES ('7401', 'Kabupaten Buton', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7402', 'Kabupaten Muna', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7403', 'Kabupaten Konawe', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7404', 'Kabupaten Kolaka', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7405', 'Kabupaten Konawe Selatan', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7406', 'Kabupaten Bombana', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7407', 'Kabupaten Wakatobi', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7408', 'Kabupaten Kolaka Utara', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7409', 'Kabupaten Buton Utara', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7410', 'Kabupaten Konawe Utara', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7471', 'Kota Kendari', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7472', 'Kota Baubau', '74', '1', null, null);
INSERT INTO `cities` VALUES ('7501', 'Kabupaten Boalemo', '75', '1', null, null);
INSERT INTO `cities` VALUES ('7502', 'Kabupaten Gorontalo', '75', '1', null, null);
INSERT INTO `cities` VALUES ('7503', 'Kabupaten Pohuwato', '75', '1', null, null);
INSERT INTO `cities` VALUES ('7504', 'Kabupaten Bone Bolango', '75', '1', null, null);
INSERT INTO `cities` VALUES ('7505', 'Kabupaten Gorontalo Utara', '75', '1', null, null);
INSERT INTO `cities` VALUES ('7571', 'Kota Gorontalo', '75', '1', null, null);
INSERT INTO `cities` VALUES ('7601', 'Kabupaten Majene', '76', '1', null, null);
INSERT INTO `cities` VALUES ('7602', 'Kabupaten Polewali Mandar', '76', '1', null, null);
INSERT INTO `cities` VALUES ('7603', 'Kabupaten Mamasa', '76', '1', null, null);
INSERT INTO `cities` VALUES ('7604', 'Kabupaten Mamuju', '76', '1', null, null);
INSERT INTO `cities` VALUES ('7605', 'Kabupaten Mamuju Utara', '76', '1', null, null);
INSERT INTO `cities` VALUES ('8101', 'Kabupaten Maluku Tenggara Barat', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8102', 'Kabupaten Maluku Tenggara', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8103', 'Kabupaten Maluku Tengah', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8104', 'Kabupaten Buru', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8105', 'Kabupaten Kepulauan Aru', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8106', 'Kabupaten Seram Bagian Barat', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8107', 'Kabupaten Seram Bagian Timur', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8108', 'Kabupaten Maluku Barat Daya', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8109', 'Kabupaten Buru Selatan', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8171', 'Kota Ambon', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8172', 'Kota Tual', '81', '1', null, null);
INSERT INTO `cities` VALUES ('8201', 'Kabupaten Halmahera Barat', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8202', 'Kabupaten Halmahera Tengah', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8203', 'Kabupaten Kepulauan Sula', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8204', 'Kabupaten Halmahera Selatan', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8205', 'Kabupaten Halmahera Utara', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8206', 'Kabupaten Halmahera Timur', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8207', 'Kabupaten Pulau Morotai', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8271', 'Kota Ternate', '82', '1', null, null);
INSERT INTO `cities` VALUES ('8272', 'Kota Tidore Kepulauan', '82', '1', null, null);
INSERT INTO `cities` VALUES ('9101', 'Kabupaten Fakfak', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9102', 'Kabupaten Kaimana', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9103', 'Kabupaten Teluk Wondama', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9104', 'Kabupaten Teluk Bintuni', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9105', 'Kabupaten Manokwari', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9106', 'Kabupaten Sorong Selatan', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9107', 'Kabupaten Sorong', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9108', 'Kabupaten Raja Ampat', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9109', 'Kabupaten Tambrauw', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9110', 'Kabupaten Maybrat', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9171', 'Kota Sorong', '91', '1', null, null);
INSERT INTO `cities` VALUES ('9401', 'Kabupaten Merauke', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9402', 'Kabupaten Jayawijaya', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9403', 'Kabupaten Jayapura', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9404', 'Kabupaten Nabire', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9408', 'Kabupaten Kepulauan Yapen', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9409', 'Kabupaten Biak Numfor', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9410', 'Kabupaten Paniai', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9411', 'Kabupaten Puncak Jaya', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9412', 'Kabupaten Mimika', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9413', 'Kabupaten Boven Digoel', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9414', 'Kabupaten Mappi', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9415', 'Kabupaten Asmat', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9416', 'Kabupaten Yahukimo', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9417', 'Kabupaten Pegunungan Bintang', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9418', 'Kabupaten Tolikara', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9419', 'Kabupaten Sarmi', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9420', 'Kabupaten Keerom', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9426', 'Kabupaten Waropen', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9427', 'Kabupaten Supiori', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9428', 'Kabupaten Mamberamo Raya', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9429', 'Kabupaten Nduga', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9430', 'Kabupaten Lanny Jaya', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9431', 'Kabupaten Mamberamo Tengah', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9432', 'Kabupaten Yalimo', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9433', 'Kabupaten Puncak', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9434', 'Kabupaten Dogiyai', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9435', 'Kabupaten Intan Jaya', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9436', 'Kabupaten Deiyai', '94', '1', null, null);
INSERT INTO `cities` VALUES ('9471', 'Kota Jayapura', '94', '1', null, null);

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `commentable_id` int(10) unsigned DEFAULT NULL,
  `commentable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_user_id_index` (`user_id`),
  KEY `comments_commentable_id_index` (`commentable_id`),
  KEY `comments_commentable_type_index` (`commentable_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2016_06_06_221310_alter_users_table_add_fields', '2');
INSERT INTO `migrations` VALUES ('2016_06_06_222243_create_provinces_table', '3');
INSERT INTO `migrations` VALUES ('2016_06_06_222254_create_cities_table', '3');
INSERT INTO `migrations` VALUES ('2016_06_06_231646_add_role_to_users_table', '4');
INSERT INTO `migrations` VALUES ('2016_06_06_233946_create_address_table', '5');
INSERT INTO `migrations` VALUES ('2016_06_07_005710_add_zipcode_to_addresses_table', '6');
INSERT INTO `migrations` VALUES ('2016_06_07_013047_add_nickname_to_users_table', '7');
INSERT INTO `migrations` VALUES ('2016_06_15_004548_create_news_table', '8');
INSERT INTO `migrations` VALUES ('2015_11_06_000000_create_comments_table', '9');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of news
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `province_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `provinces_province_name_unique` (`province_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of provinces
-- ----------------------------
INSERT INTO `provinces` VALUES ('11', 'Aceh', null, null);
INSERT INTO `provinces` VALUES ('12', 'Sumatera Utara', null, null);
INSERT INTO `provinces` VALUES ('13', 'Sumatera Barat', null, null);
INSERT INTO `provinces` VALUES ('14', 'Riau', null, null);
INSERT INTO `provinces` VALUES ('15', 'Jambi', null, null);
INSERT INTO `provinces` VALUES ('16', 'Sumatera Selatan', null, null);
INSERT INTO `provinces` VALUES ('17', 'Bengkulu', null, null);
INSERT INTO `provinces` VALUES ('18', 'Lampung', null, null);
INSERT INTO `provinces` VALUES ('19', 'Kepulauan Bangka Belitung', null, null);
INSERT INTO `provinces` VALUES ('21', 'Kepulauan Riau', null, null);
INSERT INTO `provinces` VALUES ('31', 'DKI Jakarta', null, null);
INSERT INTO `provinces` VALUES ('32', 'Jawa Barat', null, null);
INSERT INTO `provinces` VALUES ('33', 'Jawa Tengah', null, null);
INSERT INTO `provinces` VALUES ('34', 'DI Yogyakarta', null, null);
INSERT INTO `provinces` VALUES ('35', 'Jawa Timur', null, null);
INSERT INTO `provinces` VALUES ('36', 'Banten', null, null);
INSERT INTO `provinces` VALUES ('51', 'Bali', null, null);
INSERT INTO `provinces` VALUES ('52', 'Nusa Tenggara Barat', null, null);
INSERT INTO `provinces` VALUES ('53', 'Nusa Tenggara Timur', null, null);
INSERT INTO `provinces` VALUES ('61', 'Kalimantan Barat', null, null);
INSERT INTO `provinces` VALUES ('62', 'Kalimantan Tengah', null, null);
INSERT INTO `provinces` VALUES ('63', 'Kalimantan Selatan', null, null);
INSERT INTO `provinces` VALUES ('64', 'Kalimantan Timur', null, null);
INSERT INTO `provinces` VALUES ('65', 'Kalimantan Utara', null, null);
INSERT INTO `provinces` VALUES ('71', 'Sulawesi Utara', null, null);
INSERT INTO `provinces` VALUES ('72', 'Sulawesi Tengah', null, null);
INSERT INTO `provinces` VALUES ('73', 'Sulawesi Selatan', null, null);
INSERT INTO `provinces` VALUES ('74', 'Sulawesi Tenggara', null, null);
INSERT INTO `provinces` VALUES ('75', 'Gorontalo', null, null);
INSERT INTO `provinces` VALUES ('76', 'Sulawesi Barat', null, null);
INSERT INTO `provinces` VALUES ('81', 'Maluku', null, null);
INSERT INTO `provinces` VALUES ('82', 'Maluku Utara', null, null);
INSERT INTO `provinces` VALUES ('91', 'Papua Barat', null, null);
INSERT INTO `provinces` VALUES ('94', 'Papua', null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
