<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Authenticatable
{
    use \Nicolaslopezj\Searchable\SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nickname','username', 'class','email', 'password','phone', 'photo_path', 'role',
    ];

    protected $searchable = [
        'columns' => [
            'name' => 10,
        ],
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function address()
    {
        return $this->hasMany('App\Address');
    }

    public function news()
    {
        return $this->hasMany('App\News');
    }
}
