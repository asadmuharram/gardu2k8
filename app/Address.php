<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $table = 'addresses';

    protected $fillable = [
        'city_id', 'zipcode', 'user_id','address'
    ];

    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }

    public function province()
    {
        return $this->belongsTo('App\User','province_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}