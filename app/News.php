<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Slynova\Commentable\Traits\Commentable;

class News extends BaseModel
{
	use Commentable;
    protected $table = 'news';
    protected $fillable = [
        'subject', 'body', 'publish','created_by'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
