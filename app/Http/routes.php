<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\News;
use App\Auth;
use Slynova\Commentable\Models\Comment;
use Illuminate\Http\Request;

Route::group(['middlewareGroups' => ['web']], function () {

Route::auth();
Route::post('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@postRegister']);
Route::get('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::post('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard/class/{classId}', 'DashboardController@showClass');
Route::get('/dashboard/user/{username}', 'DashboardController@showUser');
Route::get('/dashboard/search', 'DashboardController@search');
Route::get('/dashboard/settings/account', 'DashboardController@settingsAccount');
Route::get('/dashboard/settings/profile', 'DashboardController@settingsProfile');
Route::put('/dashboard/settings/account/update', 'DashboardController@settingsAccountUpdate');
Route::put('/dashboard/settings/profile/update', 'DashboardController@settingsProfileUpdate');

Route::get('/dashboard/news', 'NewsController@index');
Route::get('/dashboard/news/create', 'NewsController@create');
Route::post('/dashboard/news/store', 'NewsController@store');
Route::get('/dashboard/news/{id}/edit', 'NewsController@edit');
Route::put('/dashboard/news/{id}/update', 'NewsController@update');


Route::get('dashboard/news/{id}', function($id) {

	$News = News::findOrFail($id);
	return view('backend.newsShow', compact('News'));
})->name('news.show');

Route::post('dashboard/news/{id}/comments', function($id, Request $request) {

    $News = News::findOrFail($id);
    // Correct way to do it.
    // $article->comments()->create($request->all());
    $comment = new Comment;
    $comment->user_id = $request->input('user_id');
    $comment->body = $request->input('body');
    $News->comments()->save($comment);
    return redirect()->route('news.show', [$News->id]);
})->name('news.comment');

Route::post('dashboard/news/comments/{id}/reply', function($id, Request $request)
{
    $comment = Comment::findOrFail($id);
    // Correct way to do it.
    // $newComment = Comment::create($request->all());
    $newComment = new Comment;
    $comment->user_id = $request->input('user_id');
    $newComment->body = $request->input('body');
    $newComment->save();
    $newComment->makeChildOf($comment);
    return redirect()->back();
})->name('comments.reply');

Route::get('/', 'FrontendController@index');
Route::post('/yearcheck', 'FrontendController@yearCheck');
Route::get('/oops', 'FrontendController@oops');



/* Front Ajax */
Route::get('/getcities', 'FrontendController@ajaxCities');

});