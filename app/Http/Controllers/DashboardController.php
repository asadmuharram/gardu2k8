<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use App\City;
use App\Province;
use App\Address;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;
use Hash;
use Session;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $User = User::where('id',Auth::user()->id)->first();
        $UserClass = User::where('class',Auth::user()->class)->get();

        $UserClassCount = count($UserClass) - 1;
        return view('backend.index',[
                'User' => $User,
                'UserClassCount' => $UserClassCount
            ]);
    }

    public function showClass($classId)
    {
        $User = User::where('id',Auth::user()->id)->first();
        $Users = User::where('class',$classId)->with('address.city.province')->get();
        return view('backend.class',[
                'User' => $User,
                'Users' => $Users,
                'classId' => $classId,
            ]);
    }

    public function showUser($usernameId)
    {
        $User = User::where('username',$usernameId)->with('address.city.province')->first();
        return view('backend.user',[
                'User' => $User,
            ]);
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
        $Users = User::search($keyword)->get();

        return view('backend.search',[
                'Users' => $Users,
                'keyword' => $keyword,
            ]);
    }

    public function settingsAccount()
    {

        $User = User::where('id',Auth::user()->id)->first();
        return view('backend.settingsAccount',[
                'User' => $User,
            ]);
    }

    public function settingsProfile()
    {
        $User = User::where('id',Auth::user()->id)->with('address.city.province')->first();
        return view('backend.settingsProfile',[
                'User' => $User,
            ]);
    }

    public function settingsAccountUpdate(Request $request)
    {
        $User = User::findOrFail(Auth::user()->id);

        $validator = $this->validate($request,[
            'old_password' => 'min:6',
            'password' => 'min:6|confirmed',
        ]);

        if ($User->username == $request->input('username')) {
            $validator = $this->validate($request,[
                'username' => 'required|max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'username' => 'required|max:64|unique:users',
            ]);
        }

        if ($request->has('password')) {
            $validator = $this->validate($request,[
                'password_confirmation' => 'required|min:6',
            ]);
        } else {
            $validator = $this->validate($request,[
                'password_confirmation' => 'min:6',
            ]);
        }


        if ($request->has('password')) {
            if (Hash::check($request['oldpassword'], $User->password)) {
                $User->password =  Hash::make($request['password']);
                $User->save();
                Session::flash('flash_message', 'Password berhasil diubah');
                return redirect('dashboard/settings/account');
            } else {
                Session::flash('error_message', 'Password salah');
                return redirect('dashboard/settings/account');
            }
        } else {
            $User->username =  $request['username'];
            $User->save();
            Session::flash('flash_message', 'Username berhasil diubah');
            return redirect('dashboard/settings/account');
        }
    }

    public function settingsProfileUpdate(Request $request)
    {

        $validator = $this->validate($request,[
            'name' => 'required|max:255',
            'nickname' => 'required|max:255',
            'class' => 'required',
            'address' => 'required|max:255',
            'phone' => 'required|max:20',
            'city' => 'required|max:255',
            'zipcode' => 'required',
        ]);

        if ($request->hasFile('photo')) {

            if ($request->file('photo')->isValid()) {

                $image = Input::file('photo');
                $img = Image::make($image);
                $imgSize = $img->filesize();

                if ($imgSize >= 1024000) {
                    Session::flash('error_message', 'Ukuran gambar > 1MB');
                    return redirect()->back()->withInput();
                
                } else {

                    $image = Input::file('photo');
                    $FileExt = $image->getClientOriginalExtension();
                    $filename = date('Y-m-d')."-".$request->input('username').".".$FileExt;
                    $destinationPath = 'uploads/photos/';
                    Input::file('photo')->move($destinationPath, $filename);

                    $User = User::findOrFail(Auth::user()->id);
                    $User->name = $request->input('name');
                    $User->nickname = $request->input('nickname');
                    $User->class = $request->input('class');
                    $User->phone = $request->input('phone');
                    $User->photo_path = $filename;
                    $User->save();

                    $City = City::where('id',$request->input('city'))->first(); 

                    $Address = Address::where('user_id',Auth::user()->id)->first();
                    $Address->city_id = $request->input('city');
                    $Address->province_id = $City->province_id;
                    $Address->user_id = $User->id;
                    $Address->address = $request->input('address');
                    $Address->zipcode = $request->input('zipcode');
                    $Address->save();

                    Session::flash('flash_message', 'Update berhasil!');
                    return redirect('dashboard/settings/profile');

                }

        } else {

            Session::flash('error_message', 'Gambar tidak valid');
            return redirect()->back();

        }

        } else {
            $User = User::findOrFail(Auth::user()->id);
            $User->name = $request->input('name');
            $User->nickname = $request->input('nickname');
            $User->class = $request->input('class');
            $User->phone = $request->input('phone');
            $User->phone = $request->input('phone');
            $User->save();

            $City = City::where('id',$request->input('city'))->first(); 

            $Address = Address::where('user_id',Auth::user()->id)->first();
            $Address->city_id = $request->input('city');
            $Address->province_id = $City->province_id;
            $Address->user_id = $User->id;
            $Address->address = $request->input('address');
            $Address->zipcode = $request->input('zipcode');
            $Address->save();

            Session::flash('flash_message', 'Update berhasil!');
            return redirect('dashboard/settings/profile');
        }
    }
}
