<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\City;
use App\Province;
use App\Address;

class FrontendController extends Controller
{


    public function index(Request $request)
    {

    	if ($request->hasCookie('access')) {
    		if ($request->cookie('access')) {
    			return redirect('/login');
    		} else {
    			return redirect('/oops');
    		}
    	} else {
    		return view('frontend.home');
    	}
    	
    }

    public function yearCheck(Request $request)
    {
    	$validator = $this->validate($request,[
            'year' => 'required',
        ]);

    	if ($request->input('year') == 2008) {
    		return redirect('login')->withCookie(cookie()->forever('access', true));
    	} else {
    		return redirect('/oops')->withCookie(cookie()->forever('access', false));;
    	}
    }

    public function oops()
    {
    	return view('frontend.oops');
    }

    public function ajaxCities(Request $request)
    {
        $term = $request->term ?: '';
        $cities = City::with('province')->where('city_name', 'like', '%'.$term.'%')->get();
        $valid_cities = array();
        foreach ($cities as $id => $city) {
            $valid_cities[] = array('city_id' => $city->id, 'city_name' => $city->city_name.' , '.$city->province->province_name);
        }
        return \Response::json($valid_cities);
    }
}
