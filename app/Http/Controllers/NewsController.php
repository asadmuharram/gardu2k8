<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Http\Requests;
use Auth;
use Session;
use Crypt;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function checkAdmin()
    {
        if (Auth::user()->role == 'admin') {
            return true;
        }
    }

    public function index()
    {
        if ($this->checkAdmin()) {
            $News = News::simplePaginate(10);
            return view('backend.newsIndex',[
                'News' => $News
            ]);
        } else {
            return redirect('/dashboard');
        }
        
    }

    

    public function create()
    {
        if ($this->checkAdmin()) {
        return view('backend.newsCreate',[
                
            ]);
        } else {
            return redirect('/dashboard');
        } 
    }

    public function store(Request $request)
    {
    	$validator = $this->validate($request,[
            'subject' => 'required|max:255',
            'body' => 'required|max:255',
            'publish' => 'required',
        ]);

        $News = New News();
        $News->subject = $request->input('subject');
        $News->body = $request->input('body');
        $News->publish = $request->input('publish');
        $News->user_id = Auth::user()->id;
        $News->save();

        Session::flash('flash_message', 'Pengumuman Berhasil dibuat!');
        return redirect('/dashboard/news');
    }

    public function edit($id)
    {
        if ($this->checkAdmin()) {
        $News = News::findOrFail($id);
        return view('backend.newsEdit',[
                'News' => $News
            ]);
        } else {
            return redirect('/dashboard');
        }
    }

    public function update($id,Request $request)
    {
    	$validator = $this->validate($request,[
            'subject' => 'required|max:255',
            'body' => 'required|max:255',
            'publish' => 'required',
        ]);

        $News = News::findOrFail($id);
        $News->subject = $request->input('subject');
        $News->body = $request->input('body');
        $News->publish = $request->input('publish');
        $News->save();
        Session::flash('flash_message', 'Pengumuman berhasil diubah');
        return redirect('/dashboard/news');
    }
}
