<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Address;
use App\City;
use App\Province;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Session;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'nickname' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'class' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'address' => 'required|max:255',
            'phone' => 'required|max:20',
            'city' => 'required|max:255',
            'photo' => 'required',
            'zipcode' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
    }

    protected function validatorLogin(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    protected function create(array $data)
    {
        $User = User::create([
            'name' => $data['name'],
            'nickname' => $data['nickname'],
            'email' => $data['email'],
            'class' => $data['class'],
            'username' => $data['username'],
            'phone' => $data['phone'],
            'role' => 'user',
            'password' => bcrypt($data['password']),
        ]);
        return $User;
    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($request->file('photo')->isValid()) {

                $image = Input::file('photo');
                $img = Image::make($image);
                $imgSize = $img->filesize();

                if ($imgSize >= 1024000) {
                    Session::flash('error_message', 'Ukuran gambar > 1MB');
                    return redirect()->back()->withInput();
                
                } else {

                    $image = Input::file('photo');
                    $FileExt = $image->getClientOriginalExtension();
                    $filename = date('Y-m-d')."-".$request->input('username').".".$FileExt;
                    $destinationPath = 'uploads/photos/';
                    Input::file('photo')->move($destinationPath, $filename);

                    $regisUser = $this->create($request->all());
                    $User  = User::where('id',$regisUser->id)->first();
                    $User->photo_path = $filename;
                    $User->save();

                    $City = City::where('id',$request->input('city'))->first(); 

                    $Address = new Address();
                    $Address->city_id = $request->input('city');
                    $Address->province_id = $City->province_id;
                    $Address->user_id = $User->id;
                    $Address->address = $request->input('address');
                    $Address->zipcode = $request->input('zipcode');
                    $Address->save();

                    $data = [ 
                    'firstName' => $request->input('name'),
                    'username' => $request->input('username')
                    ];

                    Mail::send('emails.welcome', $data, function($message) {
                        $message->to('muaramasad@gmail.com', 'Gardu2k8')->subject('Selamat Datang di Gardu2K8. Silahkan verifikasi emailmu.');
                    });

                    Session::flash('flash_message', 'Registrasi berhasil! , Selamat datang di Gardu2k8.');
                    Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]); 
                    return redirect('/dashboard');

                }

        } else {

            Session::flash('error_message', 'Gambar tidak valid');
            return redirect()->back();

        }

    }

    public function postLogin(Request $request)
    {   
        $validator = $this->validatorLogin($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        {
            return redirect('/dashboard');
        } else {
            Session::flash('error_message', 'Email atau Password Salah.');
            return redirect('/login');
        }

    }

    public function showLoginForm(Request $request)
    {

        if ($request->hasCookie('access')) {
            if ($request->cookie('access')) {
                return view('auth.login');
            } else {
                return redirect('/oops');
            }
        } else {
            return redirect('/');
        }
        
    }

    public function showRegistrationForm(Request $request)
    {
        if ($request->hasCookie('access')) {
            if ($request->cookie('access')) {
                return view('auth.register');
            } else {
                return redirect('/oops');
            }
        } else {
            return redirect('/');
        }
    }
}
