<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

Carbon::setLocale('id');

class BaseModel extends Model {

    public function getCreatedAtAttribute($attr) {        
        return Carbon::parse($attr)->diffForHumans(); //Change the format to whichever you desire
    }
}
