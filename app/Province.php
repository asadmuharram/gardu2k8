<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';

    public function address()
    {
        return $this->hasMany('App\Address');
    }

    public function city()
    {
        return $this->hasMany('App\City');
    }
}
